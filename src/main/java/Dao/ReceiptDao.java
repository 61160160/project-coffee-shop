/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Product;
import Model.Receipt;
import Model.Product;
import Model.User;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Receipt (TotalPrice,UserId,Cash,Change) VALUES (?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, object.getTotal());
            stmt.setInt(2, object.getUser().getId());
            stmt.setDouble(3, object.getCash());
            stmt.setDouble(4, object.getChange());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT r.Id as id,\n"
                    + "       UserId,\n"
                    + "       u.UserName as userName,\n"
                    + "       u.Password as userPass,\n"
                    + "       u.Name as name,\n"
                    + "       u.Tel as UserTel,\n"
                    + "       u.WorkTime as workTime,\n"
                    + "       u.HireDate as hireDate,\n"
                    + "       Date,\n"
                    + "       TotalPrice,\n"
                    + "       Cash,\n"
                    + "       Change\n"
                    + "  FROM Receipt r, User u \n"
                    + "  WHERE r.UserId = u.Id \n"
                    + "  ORDER BY Date DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Date")); //Throws exception
                int userId = result.getInt("UserId");
                String userName = result.getString("userName");
                String userPassword = result.getString("userPass");
                String name = result.getString("name");
                String userTel = result.getString("UserTel");
                String workTime = result.getString("workTime");
                String hireDate = result.getString("hireDate");
                Double total = result.getDouble("TotalPrice");
                Double cash = result.getDouble("Cash");
                Double change = result.getDouble("Change");

                Receipt Receipt = new Receipt(id,
                        new User(userId, userName, userPassword, name, userTel, workTime, hireDate), date, cash
                );
                list.add(Receipt);
            }

        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT r.Id as id,\n"
                    + "       UserId,\n"
                    + "       u.UserName as userName,\n"
                    + "       u.Password as userPass,\n"
                    + "       u.Name as name,\n"
                    + "       u.Tel as UserTel,\n"
                    + "       u.WorkTime as workTime,\n"
                    + "       u.HireDate as hireDate,\n"
                    + "       Date,\n"
                    + "       TotalPrice,\n"
                    + "       Cash,\n"
                    + "       Change \n"
                    + "  FROM Receipt r, User u \n"
                    + "  WHERE  r.UserId = u.Id AND r.Id =" + id;

            Statement stmt = conn.createStatement();
//            stmt.setInt(1, 1);
            ResultSet result = stmt.executeQuery(sql);
//            System.out.println(result);
            if (result.next()) {
                int rid = result.getInt("id");
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Date")); //Throws exception
                int userId = result.getInt("UserId");
                String userName = result.getString("userName");
                String userPassword = result.getString("userPass");
                String name = result.getString("name");
                String userTel = result.getString("UserTel");
                String workTime = result.getString("workTime");
                String hireDate = result.getString("hireDate");
                Double total = result.getDouble("TotalPrice");
                Double cash = result.getDouble("Cash");
                Double change = result.getDouble("Change");

                Receipt Receipt = new Receipt(rid,
                        new User(userId, userName, userPassword, name, userTel, workTime, hireDate), date, cash
                );
                db.close();
                return Receipt;
            }

//        } catch (SQLException ex) {
//            System.out.println("Error : Unable to select receipt id " + id + "!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt" + ex.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Receipt WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row: " + row);

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delete receipt id " + id + "!!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        //insert data
        try {
            String sql = "UPDATE Receipt SET UserId = ?,TotalPrice = ?,Cash = ?,Change = ? WHERE Id = ?; ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUser().getId());
            stmt.setDouble(2, object.getTotal());
            stmt.setDouble(3, object.getCash());
            stmt.setDouble(4, object.getChange());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    //test
    public static void main(String[] args) {
        Product p1 = new Product(1, "Espresso", 50, null);
        Product p2 = new Product(2, "Green Tea", 40, null);
//        User seller = new User(1, "Chakris Kulvitdamrongkul", "0123456789",);
        User seller = new User(3, "pp1122", "123456789", "Gurawan", "081-2345678", "22-22-22", "2-2-2");
        Receipt receipt = new Receipt(seller, 1000);
        receipt.addReceiptDetail(p1, 10);
        receipt.addReceiptDetail(p2, 11);
        ReceiptDao dao = new ReceiptDao();
//        int id = dao.add(receipt);
//        System.out.println(id);
        System.out.println(dao.get(23));
//        System.out.println(dao.delete(12));
//        System.out.println(dao.update(new Receipt(2, new User(2, "sd","000000","333-33","33-33"), null, 2000)));

    }
}
