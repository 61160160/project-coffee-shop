/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.User;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class UserDao implements DaoInterface<User>{

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO User (Username, Password, Name, Tel, HireDate, WorkTime) VALUES (?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getUserName());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getName());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getWorktime());
            stmt.setString(6, object.getHiredate());
            
            int row =stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if(result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Id, Username, Password, Name, Tel, HireDate, WorkTime FROM User";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int id = result.getInt("Id");
                String username = result.getString("Username");
                String password = result.getString("Password");
                String name = result.getString("Name");
                String tel = result.getString("Tel");
                String hiredate = result.getString("HireDate");
                String worktime = result.getString("WorkTime");
                User user = new User(id, username, password, name, tel, hiredate, worktime );
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Id, Username, Password, Name, Tel, HireDate, WorkTime FROM User WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
                int pid = result.getInt("Id");
                String username = result.getString("Username");
                String password = result.getString("Password");
                String name = result.getString("Name");
                String tel = result.getString("Tel");
                String hiredate = result.getString("HireDate");
                String worktime = result.getString("WorkTime");
                User user = new User(pid, username, password, name, tel, hiredate, worktime );
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
       Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM User WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row =stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        //Id, Name, Tel, HireDate, WorkTime
        try {
            String sql = "UPDATE User SET Username = ?, Password = ?, Name = ?, Tel = ?, HireDate = ?, WorkTime = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getUserName());
            stmt.setString(2, object.getPassword());
            stmt.setString(3, object.getName());
            stmt.setString(4, object.getTel());
            stmt.setString(5, object.getWorktime());
            stmt.setString(6, object.getHiredate());
            stmt.setInt(7, object.getId());
            row =stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }
    
}
