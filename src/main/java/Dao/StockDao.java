/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Stock;
import TestStock.TestSelectStock;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PP
 */
public class StockDao implements TestDaoInterface<Stock> {

    @Override
    public int add(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Stock (Name,Amount) VALUES(?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
//            Stock stock = new Stock(-1, "Sweetened Condensed Milk", 14);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
            return id;
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return -1;
    }

    @Override
    public ArrayList<Stock> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Id,Name,Amount FROM Stock";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                int amount = result.getInt("amount");
                Stock stock = new Stock(id, name, amount);
                list.add(stock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Stock get(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Id,Name,Amount FROM Stock WHERE id= " + id;
//            String sql = "SELECT Id,Name,Amount FROM Stock WHERE Name= " + name;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int sid = result.getInt("id");
                String sname = result.getString("name");
                int amount = result.getInt("amount");
                Stock stock = new Stock(sid, sname, amount);
                return stock;

            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Stock WHERE Id = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Stock object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Stock SET Name = ?, Amount = ? WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setInt(2, object.getAmount());
            stmt.setInt(3, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    public static void main(String[] args) {
        StockDao dao = new StockDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
//        int id  =dao.add(new Stock(-1,"Green tea powder", 16));
//        System.out.println("id: " + id);
//        Stock lastStock = dao.get(nname);
//        System.out.println("Last Stockitem: "+lastStock);
//        lastStock.setAmount(14);
//        int row  = dao.update(lastStock);
//        Stock updateStock = dao.get(id);
//        System.out.println("Update Stock"+updateStock);
//        dao.delete(id);
//        Stock deleteStock = dao.get(id);
//        System.out.println("delete Stock " + deleteStock);
    }

    public StockDao get() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
