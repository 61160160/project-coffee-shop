/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Model.Product;
import Model.Receipt;
import Model.ReceiptDetail;
import Model.User;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows
 */
public class ReceiptDetailDao implements DaoInterface<ReceiptDetail> {

    @Override
    public int add(ReceiptDetail object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO ReceiptDetail (ProductId,ReceiptId,Name,Price,TotalPrice,Amount) VALUES (?,?,?,?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getProduct().getId());
            stmt.setInt(2, object.getReceipt().getId());
            stmt.setString(3, object.getProduct().getName());
            stmt.setDouble(4, object.getProduct().getPrice());
            stmt.setDouble(5, object.getTotalPrice());
            stmt.setInt(6, object.getAmount());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<ReceiptDetail> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT rd.Id as id,\n"
                    + "       p.Id as productId,\n"
                    + "       re.Id as receiptId,\n"
                    + "       u.Id as userId,\n"
                    + "       u.UserName as userName,\n"
                    + "       u.Password as userPass,\n"
                    + "       u.Name as uname,\n"
                    + "       u.Tel as UserTel,\n"
                    + "       u.WorkTime as workTime,\n"
                    + "       u.HireDate as hireDate,\n"
                    + "       p.Name as name,\n"
                    + "       p.Price as price,\n"
                    + "       p.Type as type,\n"
                    + "       re.Date as date,\n"
                    + "       rd.TotalPrice,\n"
                    + "       rd.Amount\n"
                    + "  FROM ReceiptDetail rd, Receipt re ,Product p , User u\n"
                    + "  WHERE rd.ReceiptId = re.Id AND rd.ProductId = p.Id AND re.UserId = u.Id"
                    + "  ORDER BY Date DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int productid = result.getInt("productId");
                int receiptid = result.getInt("receiptId");
                int userid = result.getInt("userId");
                String userName = result.getString("userName");
                String userPassword = result.getString("userPass");
                String uname = result.getString("uname");
                String userTel = result.getString("UserTel");
                String workTime = result.getString("workTime");
                String hireDate = result.getString("hireDate");
                String name = result.getString("name");
                Double price = result.getDouble("price");
                Double total = result.getDouble("TotalPrice");
                int amount = result.getInt("Amount");
                String type = result.getString("type");

                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("date")); //Throws exception
                ReceiptDetail ReceiptDetail = new ReceiptDetail(id,
                        new Product(productid, name, price, type),
                        new Receipt(receiptid,
                                new User(userid, userName, userPassword, uname, userTel, workTime, hireDate),
                                date, 0), amount);
                list.add(ReceiptDetail);
            }

        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt" + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public ReceiptDetail get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT rd.Id as id,\n"
                    + "       p.Id as productId,\n"
                    + "       re.Id as receiptId,\n"
                    + "       u.Id as userId,\n"
                    + "       u.UserName as userName,\n"
                    + "       u.Password as userPass,\n"
                    + "       u.Name as uname,\n"
                    + "       u.Tel as UserTel,\n"
                    + "       u.WorkTime as workTime,\n"
                    + "       u.HireDate as hireDate,\n"
                    + "       p.Name as name,\n"
                    + "       p.Price as price,\n"
                    + "       p.Type as type,\n"
                    + "       re.Date as date,\n"
                    + "       rd.TotalPrice,\n"
                    + "       rd.Amount\n"
                    + "  FROM ReceiptDetail rd, Receipt re ,Product p , User u\n"
                    + "  WHERE rd.ReceiptId = re.Id AND rd.ProductId = p.Id AND re.UserId = u.Id AND rd.Id =" + id;

            Statement stmt = conn.createStatement();
//            stmt.setInt(1, 1);
            ResultSet result = stmt.executeQuery(sql);
//            System.out.println(result);
            if (result.next()) {
                int rdid = result.getInt("id");
                int productid = result.getInt("productId");
                int receiptid = result.getInt("receiptId");
                int userid = result.getInt("userId");
                String userName = result.getString("userName");
                String userPassword = result.getString("userPass");
                String uname = result.getString("uname");
                String userTel = result.getString("UserTel");
                String workTime = result.getString("workTime");
                String hireDate = result.getString("hireDate");
                String name = result.getString("name");
                Double price = result.getDouble("price");
                Double total = result.getDouble("TotalPrice");
                int amount = result.getInt("Amount");
                String type = result.getString("type");

                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("date")); //Throws exception

                ReceiptDetail ReceiptDetail = new ReceiptDetail(rdid,
                        new Product(productid, name, price, type),
                        new Receipt(receiptid,
                                new User(userid, userName, userPassword, uname, userTel, workTime, hireDate),
                                date, 0), amount);
                db.close();
                return ReceiptDetail;
            }

//        } catch (SQLException ex) {
//            System.out.println("Error : Unable to select receipt id " + id + "!!" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing all receipt" + ex.getMessage());
        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return null;
    }
    
    public ArrayList<ReceiptDetail> getDetail(int index) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        //read data
        try {
            String sql = "SELECT rd.Id as id,\n"
                    + "       p.Id as productId,\n"
                    + "       re.Id as receiptId,\n"
                    + "       u.Id as userId,\n"
                    + "       u.UserName as userName,\n"
                    + "       u.Password as userPass,\n"
                    + "       u.Name as uname,\n"
                    + "       u.Tel as UserTel,\n"
                    + "       u.WorkTime as workTime,\n"
                    + "       u.HireDate as hireDate,\n"
                    + "       p.Name as name,\n"
                    + "       p.Price as price,\n"
                    + "       p.Type as type,\n"
                    + "       re.Date as date,\n"
                    + "       rd.TotalPrice,\n"
                    + "       rd.Amount\n"
                    + "  FROM ReceiptDetail rd, Receipt re ,Product p , User u\n"
                    + "  WHERE rd.ReceiptId = re.Id AND rd.ProductId = p.Id AND re.UserId = u.Id AND re.id = " + index
                    + "  ORDER BY Date DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int productid = result.getInt("productId");
                int receiptid = result.getInt("receiptId");
                int userid = result.getInt("userId");
                String userName = result.getString("userName");
                String userPassword = result.getString("userPass");
                String uname = result.getString("uname");
                String userTel = result.getString("UserTel");
                String workTime = result.getString("workTime");
                String hireDate = result.getString("hireDate");
                String name = result.getString("name");
                Double price = result.getDouble("price");
                Double total = result.getDouble("TotalPrice");
                int amount = result.getInt("Amount");
                String type = result.getString("type");

                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("date")); //Throws exception
                ReceiptDetail ReceiptDetail = new ReceiptDetail(id,
                        new Product(productid, name, price, type),
                        new Receipt(receiptid,
                                new User(userid, userName, userPassword, uname, userTel, workTime, hireDate),
                                date, 0), amount);
                list.add(ReceiptDetail);
            }

        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt" + ex.getMessage());
        } catch (ParseException ex) {
            Logger.getLogger(ReceiptDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }
    
    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM ReceiptDetail WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
            System.out.println("Affect row: " + row);

        } catch (SQLException ex) {
            System.out.println("Error : Unable to delete receipt id " + id + "!!");
        }

        db.close();
        return row;
    }

    @Override
    public int update(ReceiptDetail object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        //insert data
        try {
            String sql = "UPDATE ReceiptDetail SET ProductId = ?,ReceiptId = ?,Name = ?,Price = ?,TotalPrice = ?,Amount = ? WHERE Id = ?; ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getProduct().getId());
            stmt.setInt(2, object.getReceipt().getId());
            stmt.setString(3, object.getProduct().getName());
            stmt.setDouble(4, object.getProduct().getPrice());
            stmt.setDouble(5, object.getTotalPrice());
            stmt.setInt(6, object.getAmount());
            stmt.setInt(7, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Espresso", 50, null);
        Product p2 = new Product(2, "Green Tea", 40, "d");
//        User seller = new User(1, "Chakris Kulvitdamrongkul", "0123456789",);
        User seller = new User(3, "pp1122", "123456789", "Gurawan", "081-2345678", "22-22-22", "2-2-2");
        Receipt receipt = new Receipt(seller, 1000);
        ReceiptDao rdao = new ReceiptDao();

        receipt.addReceiptDetail(p1, 10);
        ReceiptDetail r1 = new ReceiptDetail(p1, receipt, 10);
        receipt.addReceiptDetail(p2, 11);

        ReceiptDetailDao dao = new ReceiptDetailDao();
//        System.out.println(dao.add(r1));
//        System.out.println(dao.get(3));
        System.out.println(dao.getAll());
//          System.out.println(dao.delete(2));
//        System.out.println(dao.update(new ReceiptDetail(2, p2, receipt, 12)));
    }
}
