/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestStock;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PP
 */
public class TestUpdateStock {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "UPDATE Stock SET Name = ?, Amount = ? WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "Sugars");
            stmt.setInt(2, 7);
            stmt.setInt(3, 4);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }

}
