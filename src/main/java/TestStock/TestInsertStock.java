/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestStock;

import Model.Stock;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PP
 */
public class TestInsertStock {
        public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/DataStore.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Data con");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            String sql = "INSERT INTO Stock (Name,Amount) VALUES(?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Stock stock = new Stock(-1, "Sweetened Condensed Milk", 14);
            stmt.setString(1, stock.getName());
            stmt.setInt(2, stock.getAmount());
            int row =  stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1 ;
            if(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row "+ row + "id " + id);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (conn != null) {
                conn.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
