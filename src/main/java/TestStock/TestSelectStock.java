/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestStock;

import Model.Stock;
import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PP
 */
public class TestSelectStock {

    public static void main(String[] args) {
            Connection conn = null;
            Database db = Database.getInstance();
            conn = db.getConnection();
            try {
                String sql = "SELECT Id,Name,Amount FROM Stock WHERE name = ''Sweetened Condensed Milk'";
                Statement stmt = conn.createStatement();
                ResultSet result = stmt.executeQuery(sql);
                while (result.next()) {
                    int id = result.getInt("id");
                    String name = result.getString("name");
                    int amount = result.getInt("amount");
                    Stock stock = new Stock(id,name,amount);
                    System.out.println(stock);

                }
            } catch (SQLException ex) {
                Logger.getLogger(TestSelectStock.class.getName()).log(Level.SEVERE, null, ex);
            }

            db.close();
    }
}
