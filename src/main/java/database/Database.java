/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PP
 */
public class Database {

    private static Database instance = new Database();
    private Connection conn = null;

    private Database() {
    }

    public static Database getInstance() {
        String dbPath = "./db/DataStore.db";
        try {
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database connection" + instance.conn);
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exist"+ ex);
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connection");
        }
        return instance;
    }

    public static void close() {
        try {
            if (instance.conn != null || instance.conn.isClosed()) {
                instance.conn.close();
                System.out.println("Database close");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.conn = null;
    }
    public Connection getConnection(){
        return instance.conn;
    }
}
