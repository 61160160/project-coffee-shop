/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Windows
 */
public class User {
    private int id ;
    private String username,password,name,tel,worktime,hiredate ;

    public User(int id,String username,String password, String name, String tel, String worktime, String hiredate) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.tel = tel;
        this.worktime = worktime;
        this.hiredate = hiredate;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username="+ username + ", password=" + password +", name=" + name + ", tel=" + tel + ", worktime=" + worktime + ", hiredate=" + hiredate + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
     public String getUserName() {
        return username;
    }

    public void setUserName(String username) {
        this.username = username;
    }
     public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getWorktime() {
        return worktime;
    }

    public void setWorktime(String worktime) {
        this.worktime = worktime;
    }

    public String getHiredate() {
        return hiredate;
    }

    public void setHiredate(String hiredate) {
        this.hiredate = hiredate;
    }

}
