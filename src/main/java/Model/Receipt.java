/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

//import java.sql.Date;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Windows
 */
public class Receipt {

    private int id;
    private User user;
    private Date date;
    private double cash;
    private double change;
    private ArrayList<ReceiptDetail> receiptdetail;

    public Receipt(int id, User user, Date date, double cash) {
        this.id = id;
        this.user = user;
        this.date = date;
        this.cash = cash;
        receiptdetail = new ArrayList<>();
    }

    public Receipt(User user, double cash) {
        this(-1, user, null, cash);
    }

    public void addReceiptDetail(int id, Product product, int amount) {
        for (int row = 0; row < receiptdetail.size(); row++) {
            ReceiptDetail r = receiptdetail.get(row);
            if (r.getProduct().getId() == product.getId()) {
                r.addAmount(amount);
//                System.out.println(receiptdetail.get(row).toString());
                return;
            }
        }
        receiptdetail.add(new ReceiptDetail(id, product, this, amount));
//        ReceiptDetail r1 = new ReceiptDetail(product, this, amount);
//        if (receiptdetail.size() == 2) {
//            System.out.println(receiptdetail.get(1).toString());
//        }
    }

    public void addReceiptDetail(Product product, int amount) {
//        System.out.println("111");
        addReceiptDetail(receiptdetail.size()+1, product, amount);
        
    }

    public void deleteReceiptDetail(int row) {
        receiptdetail.remove(row);
    }

    public double getTotal() {
        double total = 0;
        for (ReceiptDetail r : receiptdetail) {
            total += r.getTotalPrice();
//            System.out.println(total);
        }
        return total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return cash - this.getTotal();
    }

    public void setChange(double change) {
        this.change = change;
    }
    
    
    
    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", user=" + user + ", date=" + date + ", totalprice=" + this.getTotal() + ", cash=" + cash + ", change=" + this.getChange() + "} \n";
    }

}
