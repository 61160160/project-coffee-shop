/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Windows
 */
public class ReceiptDetail {

    private int id;
    private Product product;
    private Receipt receipt;
    private double totalprice;
    private int amount;

    public ReceiptDetail(int id, Product product, Receipt receipt, int amount) {
        this.id = id;
        this.product = product;
        this.receipt = receipt;
        this.amount = amount;
    }

    public ReceiptDetail(Product product, Receipt receipt, int amount) {
        this(-1,product,receipt,amount);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public double getTotalPrice() {
        return product.getPrice() * amount;
    }

    public void setTotalPrice(double totalprice) {
        this.totalprice = totalprice;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    public void addAmount(int amount){
        this.amount += amount;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", product=" + product + ", receipt=" + receipt + ", totalPrice=" + this.getTotalPrice()+ ", amount=" + amount + '}';
    }
    
    
}
